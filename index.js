const assert = require('assert')
const { isEmpty, omit, isFunction, snakeCase, transform, pick } = require('lodash')
const { default: Config } = require('./config')
const { default: proxyHandler } = require('./proxy-handler')
const { default: Util } = require('./util')

/**
 * Base model class to be extended that basically wraps around Knex. Holds helpful static methods.
 *
 * @class
 */
class Base {
  /**
   * Get the knex to be used.
   *
   * @returns {Function} The knex to be used.
   */
  static get knex () {
    return Config.knex
  }

  /**
   * Set the knex to be used and save it for future use.
   *
   * @param k {Function} Knex to be used.
   */
  static set knex (k) {
    Config.knex = k
  }

  /**
   * Get the table used for this model.
   *
   * @returns {string} The database table used.
   */
  static get table () {
    if (!this._table) {
      this._table = Util.table(this)
    }

    return this._table
  }

  /**
   * Set the table used for this model.
   *
   * @param {string} t The database table to be used.
   */
  static set table (t) {
    this._table = t
  }

  /**
   * Get the primary or compound keys used to uniquely identify a model.
   *
   * @returns {array} The columns used to uniquely identify a model.
   */
  static get keys () {
    if (!this._keys) {
      this._keys = []
    }

    return this._keys
  }

  /**
   * Set the primary or compound keys used to uniquely identify a model.
   *
   * @param {array} k The columns used to uniquely identify a model.
   */
  static set keys (k) {
    this._keys = k
  }

  /**
   * Forges a model but does not insert. Calls the constructor under the hood.
   *
   * @param {Object} properties The Model properties.
   * @param {Object} opts Options.
   * @returns {*} An instantiation of the model.
   */
  static forge (properties = {}, opts = {}) {
    return new this(properties, opts)
  }

  /**
   * Converts object to an instance
   *
   * @param {Object} properties The Model properties.
   * @param {Object} opts Options.
   * @returns {*} An instantiation of the model.
   */
  static hydrate (properties = {}, opts = {}) {
    return new this(Util.toCamel(properties), opts)
  }

  /**
   * Inserts a new model into the database then returns an instantiation of the model.
   *
   * @param {Object} properties The Model properties.
   * @param {Object} opts Options.
   * @returns {*} An instantiation of the model.
   */
  static create (properties, opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)
    const isArray = Array.isArray(properties)
    const inserts = (isArray ? properties : [properties]).map(p => {
      const row = this.fields ? pick(p, this.fields) : p
      return Util.toSnake(row)
    })

    // If we have empty array, just bail
    if (inserts.length === 0) return Promise.resolve([])

    const q = knex(this.table).insert(inserts)

    const res = opts.noConflict
      ? knex.raw('? ON CONFLICT DO NOTHING', [q]).then(({ rows = [] }) => rows)
      : q.returning('*')

    return res.then((rows = []) => isArray
      ? rows.map(r => new this(Util.toCamel(r), opts))
      : new this(Util.toCamel(rows[0]), opts)
    )
  }

  /**
   * Selects the first record then returns an instantiation of the model.
   *
   * @param {Object} query The Query object.
   * @param {Object} opts Options.
   * @returns {*} An instantiation of the model, if it exists, null otherwise.
   */
  static fetch (query = {}, opts = {}) {
    assert((!isEmpty(query) || isFunction(query)), 'Query cannot be empty')
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)
    const q = knex(this.table)
      .first('*')
      .where(typeof query === 'function' ? query : Util.toSnake(query))

    return q.then((res) => (typeof res === 'undefined') ? undefined : new this(Util.toCamel(res), opts))
  }

  /**
   * Returns boolean of the query if records exist
   *
   * @param {Object} query The Query object.
   * @param {Object} opts Options.
   * @returns {Boolean} If results exists
   */
  static exists (query = {}, opts = {}) {
    assert((!isEmpty(query) || isFunction(query)), 'Query cannot be empty')
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)
    const q = knex(this.table)
      .select(1)
      .where(typeof query === 'function' ? query : Util.toSnake(query))

    return q.then(rows => Boolean(rows.length))
  }

  /**
   * Saves the properties currently set on the model.
   *
   * @param {Object} properties The properties to update.
   * @param {Object} query Where clause for updating.
   * @param {Object} opts Options for saving.
   *
   * @return {Promise<Array>} A collection of the updated models.
   */
  static update (_properties, query = {}, opts = {}) {
    assert((!isEmpty(query) || isFunction(query)), 'Query cannot be empty')
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)
    const prunedProperties = Util.prune(_properties)

    // If the class defines a static property called 'fields', we want to only
    // use those fields
    const properties = this.fields
      ? pick(prunedProperties, this.fields)
      : prunedProperties

    // This caused problems with an empty update and the collection overriden
    // in the model was looking for properties not includes in the query
    if (isEmpty(properties)) return this.collection(query, opts)

    const where = isFunction(query)
      ? query
      : Util.toSnake(query)

    return knex(this.table)
      .update(Util.toSnake(properties), '*')
      .where(where)
      .then((rows = []) => {
        return rows.map(res => {
          const model = new this(Util.toCamel(res), opts)
          model._safeProperties = res

          return model
        })
      })
  }

  /**
   * Deletes models matching the given where clause.
   *
   * @param {Object} query Where clause for updating.
   * @param {Object} opts Options for saving.
   * @return {Promise} A knex promise yielding rows affected.
   */
  static destroy (query = {}, opts = {}) {
    assert(!isEmpty(query), 'Query is empty')
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)

    // Delete item
    const q = knex(this.table)
      .del()
      .where(Util.toSnake(query))
    return q
  }

  /**
   * Get all the rows in a table
   *
   * @param {Object} opts Options.
   * @returns {Array} An array holding resultant models.
   */
  static getAll (opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)

    const q = knex(this.table)
      .select()

    if (opts.orderBy) {
      // Find out if using a db.raw
      const orderByMethod = Util.isRaw(opts.orderBy)
        ? 'orderByRaw'
        : 'orderBy'

      q[orderByMethod](opts.orderBy)
    }

    return q.then((rows = []) =>
      rows.map(res =>
        new this(Util.toCamel(res), opts)
      )
    )
  }

  /**
   * Get a collection of models matching a given query.
   *
   * @param {Object} query The query to match against.
   * @param {Object} opts Options.
   * @returns {Array} An array holding resultant models.
   */
  static collection (query = {}, opts = {}) {
    assert((!isEmpty(query) || isFunction(query)), 'Query cannot be empty')
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)

    const where = isFunction(query)
      ? query
      : Util.toSnake(Util.prune(query))

    const q = knex(this.table)
      .select()
      .where(where)

    if (opts.orderBy) {
      // Find out if using a db.raw
      const orderByMethod = Util.isRaw(opts.orderBy)
        ? 'orderByRaw'
        : 'orderBy'

      q[orderByMethod](opts.orderBy)
    }

    return q.then((rows = []) =>
      rows.map(res =>
        new this(Util.toCamel(res), opts)
      )
    )
  }

  static mapEntity (res, opts) {
    return new this(Util.toCamel(res), opts)
  }

  /**
   * Get a count of models matching a given query.
   *
   * @param {Object} query The query to match against.
   * @param {Object} opts Options.
   * @returns {Array} An array holding resultant models.
   */
  static count (query = {}, opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)

    const where = isFunction(query)
      ? query
      : Util.toSnake(query)

    const q = knex(this.table)
      .select()
      .count('*')
      .where(where)

    return q.then(([{ count }]) => parseInt(count, 10))
  }

  static findByRelation (relation, id, opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)
    const table = this.table
    const joinTable = snakeCase(`${relation}_${table}`)
    const q = knex(table)
      .join(joinTable, `${joinTable}.${table}_id`, `${table}.id`)
      .where(`${joinTable}.${relation}_id`, id)
      .orderBy(`${table}.${opts.orderBy || 'created_at'}`)

    return q.then((rows = []) =>
      rows.map(res =>
        new this(Util.toCamel(res), opts)
      )
    )
  }

  static listEnum (enumName, opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || Config.knex)
    return knex.raw(`
      SELECT unnest(enum_range(NULL::??)) as ??; 
    `, [enumName, enumName])
      .then(({ rows }) => rows.map(row => row[enumName]))
  }

  /**
   * Forges a new object but does not persist to DB. Does not need to be overridden.
   *
   * @constructor
   * @param {Object} properties Additional properties to set.
   * @param {Object} config A configuration object for options.
   * @returns {*} An instantiation of the model with given properties set.
   */
  constructor (properties = {}, config = {}) {
    Config.knex = config.knex || Config.knex
    const { id, ...rest } = properties
    this._trx = config.trx || config.db
    this._properties = properties
    this._safeProperties = Util.toSnake(rest)
    Object.assign(this, properties)

    return new Proxy(this, proxyHandler) // eslint-disable-line no-undef
  }

  /**
   * Saves the properties currently set on the model.
   *
   * @param {Object} opts Options for saving.
   * @return {*} An updated copy of the model.
   */
  save (opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || this._trx || Config.knex)
    const method = opts.method || 'insert'

    // Disallow unknown saving methods
    if (method !== 'insert' && method !== 'update') {
      throw new Error('Only the `insert` and `update` methods are allowed while saving.')
    }

    transform(this._properties, (result, value, key) => {
      result[snakeCase(key)] = value
    }, this._safeProperties)

    const q = knex(this.constructor.table)[method](this._safeProperties, '*')

    if (method === 'update') {
      q.where('id', this.id)
    }

    return q.then(([res]) => {
      // Save computed values
      transform(res, (result, value, key) => {
        result[key] = value
      }, this._properties)

      this._safeProperties = res

      return this
    })
  }

  /**
   * Delete the model.
   *
   * @param {Object} opts Options for saving.
   * @return {Promise} A knex promise yielding rows affected.
   */
  destroy (opts = {}) {
    const knex = Util.assertKnex(opts.trx || opts.db || this._trx || Config.knex)
    let query = { id: this.id }

    // Custom primary or compound keys
    if (this.constructor.keys.length) {
      query = transform(this.constructor.keys, (a, v) => {
        a[snakeCase(v)] = this[v]
      }, {})
    }

    return knex(this.constructor.table)
      .del()
      .where(Util.toSnake(query))
  }

  transaction (trx) {
    this._trx = trx

    return this
  }

  /**
   * Convert item into a plan object
   * FIXME: DELETEME: This is not used and was fixed when added in
   * Object.assign to the constructor
   */
  toJson ({ not } = {}) {
    return omit(this._properties, not)
  }

  /**
   * Serializes the model into JSON.
   *
   * @returns {String} The model stringified.
   */
  toString () {
    return JSON.stringify(this._properties)
  }
}

/**
 * @module
 * @type {Base}
 */
exports.default = Base
exports.Util = Util
