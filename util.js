const { Range } = require('pg-range')
const assert = require('assert')
const { camelCase, snakeCase, transform, omitBy, isUndefined } = require('lodash')

/**
 * Static utility class.
 */
class Util {
  /**
   * Static Helper method to return a pluralized table name for the model in both a static and nonstatic context.
   *
   * @returns {String} The pluralized database table's name.
   */
  static table (thiz) {
    if (thiz.constructor.name === 'Function') {
      // static context
      return snakeCase(thiz.name)
    }

    // non-static context
    return snakeCase(thiz.constructor.name)
  }

  /**
   * Helper method to convert an object into a snake_case format for use with knex.
   *
   * @param {Object} obj The object to convert.
   * @returns {Object} The resultant snake_case object.
   */
  static toSnake (obj, prefix) {
    return transform(obj, (result, value, key) => {
      const prop = prefix
        ? `${prefix}.${snakeCase(key)}`
        : snakeCase(key)
      result[prop] = value
    }, {})
  }

  /**
   * Helper method to convert an object into a camelCase format for use with after knex.
   *
   * @param {Object} obj The object to convert.
   * @returns {Object} The resultant camelCase object.
   */
  static toCamel (obj) {
    return transform(obj, (result, value, key) => {
      result[camelCase(key)] = value
    }, {})
  }

  static assertKnex (knex) {
    assert(knex, 'You must provide a knex object!')

    return knex
  }

  static prune (obj) {
    return omitBy(obj, isUndefined)
  }

  static toRange (begin, end, bounds = '[)') {
    return Range(begin, end, bounds)
  }

  static parseRange (range = '(,)') {
    return Range.super_.parse(range)
  }

  static isRaw (fn) {
    return (fn && fn.constructor && fn.constructor.name && fn.constructor.name === 'Raw')
  }
}

/**
 * @module
 * @type {Util}
 */
exports.default = Util
