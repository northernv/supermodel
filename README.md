## @northernv/supermodel

[![npm](https://img.shields.io/npm/v/@northernv/supermodel.svg?maxAge=2592000)](https://www.npmjs.com/package/@northernv/supermodel)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/shanestillwell/supermodel/raw/master/LICENSE)

Completely inspired by [knex-supermodel](https://www.npmjs.com/package/knex-supermodel), just altered with some personal modifications to meet my needs

## Usage

```
npm install -S @northernv/supermodel;
```

```
yarn add @northernv/supermodel;
```

In your models, that are Javascript classes, just `extend` with the main class

```
const { default: Base } = require('@northernv/supermodel')

class User extends Base {
}
```

## Description

`@northernv/supermodel` is meant to be a very lite but not quite an ORM for knex. This is accomplished by providing a base model that is simply an ES6 class that you extend in your own models. You can override the provided methods or even add to them to make your own. Each method will always return your model back to you!

This package requires ES6 features only available in node 6+.

## Benefits

* Converts camelCase into snake_case for inserts / updates
* Converts snake_case to camelCase automatigically

## Static Examples

Each subclass will have automatic access to static methods to `create`, `fetch`, `collection`, `forge`, `update` and `destroy`, `count`, `getAll`.

```javascript
const { default: Base } = require('@northernv/supermodel')
const knex = require('knex')
const db = knex({ ...knexOptions })

class User extends Base {
  constructor(opts) {
    super(opts);
  }
}

// Note: It converts properties to snake_case
const user = User.create({ fooBar: 'hello', barBaz: 'baz' }, { db });

console.log(user.fooBar); // hello
console.log(user.foo_bar); // hello
console.log(user.barBaz); // baz

```

## Transacting

You may either provide a transacting knex to each method or chain it.

```javascript
let user;

knex.transaction((trx) => {
  User.fetch({ id: '123' }, { db: trx })
    .then((u) => {
      user = u;

      return user.update({ foo: 'baz' });
    });
});
```

## License

This software is licensed under [the MIT license](LICENSE.md).
