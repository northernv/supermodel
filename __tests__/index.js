const k = require('knex')
const mockDb = require('mock-knex')
const { default: Base } = require('../')

const tracker = mockDb.getTracker()

const db = k({
  client: 'pg',
})
mockDb.mock(db)

class Fake extends Base {}

beforeEach(() => {
  tracker.install()
})
afterEach(() => {
  tracker.uninstall()
})

describe('create', () => {
  it('should fail when no knex object given', () => {
    expect(() => Fake.create({ name: 'none' })).toThrow()
  })
  it('should return array of objects when passed array', async () => {
    tracker.on('query', (query) => {
      query.response([{ id: 123 }, { id: 345 }])
    })
    const res = await Fake.create([{ n: 1 }, { n: 2 }], { db })
    expect(res).toHaveLength(2)
  })
  it('should handle camelCase props', async () => {
    let converted = false
    tracker.on('query', (query) => {
      if (query.sql.includes('unit_name')) converted = true
      query.response([{ unit_name: 123 }])
    })
    const res = await Fake.create({ unitName: 1 }, { db })
    expect(res.unitName).toBe(123)
    expect(converted).toBe(true)
  })
  it('should return empty array when passed empty array', async () => {
    tracker.on('query', (query) => {
      throw new Error('cant insert empty array')
    })
    const res = await Fake.create([], { db })
    expect(res).toEqual([])
  })
  it('should return a single object when passed object', async () => {
    tracker.on('query', (query) => {
      query.response([{ id: 123 }])
    })
    const res = await Fake.create({ n: 1 }, { db })
    expect(res).toEqual({ id: 123 })
  })
  it('should set ON CONFLICT DO NOTHING', async () => {
    let hasOnConflict = false
    tracker.on('query', (query) => {
      if (query.sql.includes('ON CONFLICT')) hasOnConflict = true
      query.response({ rows: [{ id: 123 }] })
    })
    const res = await Fake.create({ n: 1 }, { db, noConflict: true })
    expect(hasOnConflict).toBe(true)
    expect(res).toEqual({ id: 123 })
  })
  it('should use static fields array if defined', async () => {
    let confirmed = false
    class FakeWithFields extends Base {}
    FakeWithFields.fields = [
      'name',
    ]
    tracker.on('query', (query) => {
      if (query.sql.includes('name') && !query.sql.includes('color')) confirmed = true
      query.response([{ name: 123 }])
    })
    const res = await FakeWithFields.create({ name: 'hello', color: 'blue' }, { db })
    expect(res.name).toBe(123)
    expect(confirmed).toBe(true)
  })
})

describe('update', () => {
  it('should fail when no query given', () => {
    expect(() => Fake.update({ name: 'none' }, {}, { db })).toThrow()
    expect(() => Fake.update({ name: 'none' }, [], { db })).toThrow()
    expect(() => Fake.update({ name: 'none' }, null, { db })).toThrow()
  })
  it('should fail when no knex object given', () => {
    expect(() => Fake.update({ name: 'none' }, {})).toThrow()
  })
  it('should return array of objects', async () => {
    tracker.on('query', (query) => {
      query.response([{ id: 123 }, { id: 345 }])
    })
    const res = await Fake.update({ n: 1 }, { id: true }, { db })
    expect(res).toHaveLength(2)
  })
  it('should handle camelCase props', async () => {
    let converted = false
    tracker.on('query', (query) => {
      if (query.sql.includes('unit_name')) converted = true
      query.response([{ unit_name: 123 }])
    })
    const res = await Fake.update({ unitName: 1 }, { id: true }, { db })
    expect(res[0].unitName).toBe(123)
    expect(converted).toBe(true)
  })
  it('should use where function when passed fn', async () => {
    let hasFn = false
    tracker.on('query', (query) => {
      if (query.sql.includes('hello')) hasFn = true
      query.response([{ id: 123 }])
    })
    const fn = qb => { qb.where('hello', 'now') }
    const res = await Fake.update({ n: 1 }, fn, { db })
    expect(hasFn).toBe(true)
    expect(res[0]).toEqual({ id: 123 })
  })
  it('should use static fields array if defined', async () => {
    let confirmed = false
    class FakeWithFields extends Base {}
    FakeWithFields.fields = [
      'name',
    ]
    tracker.on('query', (query) => {
      if (query.sql.includes('name') && !query.sql.includes('color')) confirmed = true
      query.response([{ name: 123 }])
    })
    const res = await FakeWithFields.update({ name: 'hello', color: 'blue' }, { id: true }, { db })
    expect(res[0].name).toBe(123)
    expect(confirmed).toBe(true)
  })
})

describe('destroy', () => {
  it('should fail when empty query passed', () => {
    expect(() => Fake.destroy({}, { db })).toThrow()
    expect(() => Fake.destroy([], { db })).toThrow()
    expect(() => Fake.destroy(null, { db })).toThrow()
  })
  it('should fail when no knex object given', () => {
    expect(() => Fake.destroy({ name: 'none' }, {})).toThrow()
  })
  it('should return numeric number of items delete', async () => {
    tracker.on('query', (query) => {
      query.response(1)
    })
    const res = await Fake.destroy({ n: 1 }, { db })
    expect(res).toBe(1)
  })
  it('should handle camelCase props', async () => {
    let converted = false
    tracker.on('query', (query) => {
      if (query.sql.includes('unit_name')) converted = true
      query.response(1)
    })
    await Fake.destroy({ unitName: 1 }, { db })
    expect(converted).toBe(true)
  })
})

describe('fetch', () => {
  it('should fail when no knex object given', () => {
    expect(() => Fake.collection({ name: 'none' })).toThrow()
  })
  it('should fail when no query given', () => {
    expect(() => Fake.update({ name: 'none' }, {}, { db })).toThrow()
    expect(() => Fake.update({ name: 'none' }, [], { db })).toThrow()
    expect(() => Fake.update({ name: 'none' }, null, { db })).toThrow()
  })
  it('should handle camelCase props', async () => {
    let converted = false
    tracker.on('query', (query) => {
      if (query.sql.includes('unit_name')) converted = true
      query.response([{ unit_name: 123 }])
    })
    await Fake.fetch({ unitName: 1 }, { db })
    expect(converted).toBe(true)
  })
  it('should use where function when passed fn', async () => {
    let hasFn = false
    tracker.on('query', (query) => {
      if (query.sql.includes('hello')) hasFn = true
      query.response([{ id: 123 }])
    })
    const fn = qb => { qb.where('hello', 'now') }
    const res = await Fake.fetch(fn, { db })
    expect(hasFn).toBe(true)
    expect(res).toEqual({ id: 123 })
  })
})

describe('collection', () => {
  it('should fail when no knex object given', () => {
    expect(() => Fake.collection({ name: 'none' })).toThrow()
  })
  it('should return array of objects', async () => {
    tracker.on('query', (query) => {
      query.response([{ id: 1 }, { id: 345 }])
    })
    const res = await Fake.collection([{ n: 1 }, { n: 2 }], { db })
    expect(res).toHaveLength(2)
  })
  it('should handle camelCase props', async () => {
    let converted = false
    tracker.on('query', (query) => {
      if (query.sql.includes('unit_name')) converted = true
      query.response([{ unit_name: 123 }])
    })
    await Fake.collection({ unitName: 1 }, { db })
    expect(converted).toBe(true)
  })
  it('should use where function when passed fn', async () => {
    let hasFn = false
    tracker.on('query', (query) => {
      if (query.sql.includes('hello')) hasFn = true
      query.response([{ id: 123 }])
    })
    const fn = qb => { qb.where('hello', 'now') }
    const res = await Fake.collection(fn, { db })
    expect(hasFn).toBe(true)
    expect(res[0]).toEqual({ id: 123 })
  })
  it('should use orderBy', async () => {
    tracker.on('query', (query) => {
      expect(query.sql).toContain('order by "id"')
      query.response([{ id: 777 }, { id: 345 }])
    })
    const res = await Fake.collection([{ n: 1 }], { db, orderBy: 'id' })
    expect(res).toHaveLength(2)
  })
  it('should use orderBy in object form', async () => {
    tracker.on('query', (query) => {
      expect(query.sql).toContain('order by "id" asc')
      query.response([{ id: 666 }, { id: 345 }])
    })
    const res = await Fake.collection({ n: 1 }, { db, orderBy: [{ column: 'id' }] })
    expect(res).toHaveLength(2)
  })
  it('should use orderBy in array object form', async () => {
    tracker.on('query', (query) => {
      expect(query.sql).toContain('order by "id" asc, "name" desc')
      query.response([{ id: 222 }, { id: 444 }])
    })
    const res = await Fake.collection({ n: 1 }, { db, orderBy: [{ column: 'id', order: 'asc' }, { column: 'name', order: 'desc' }] })
    expect(res).toHaveLength(2)
  })
  it.todo('should use orderByRaw')
})

describe('count', () => {
  it('should fail when no knex object given', () => {
    expect(() => Fake.count({ name: 'none' })).toThrow()
  })

  it('should return a single value integer', async () => {
    tracker.on('query', (query) => {
      query.response([{ count: 123 }])
    })
    const res = await Fake.count({ n: 1 }, { db })
    expect(res).toEqual(123)
  })
})

describe('exists', () => {
  it('should fail when no knex object given', () => {
    expect(() => Fake.count({ name: 'none' })).toThrow()
  })

  it('should fail when no query given', () => {
    expect(() => Fake.update({ name: 'none' }, {}, { db })).toThrow()
    expect(() => Fake.update({ name: 'none' }, [], { db })).toThrow()
    expect(() => Fake.update({ name: 'none' }, null, { db })).toThrow()
  })

  it('should return false when empty result', async () => {
    tracker.on('query', (query) => {
      query.response([])
    })
    const res = await Fake.exists({ n: 1 }, { db })
    expect(res).toEqual(false)
  })

  it('should return true when query has results', async () => {
    tracker.on('query', (query) => {
      query.response([{ fake: 1 }])
    })
    const res = await Fake.exists({ n: 1 }, { db })
    expect(res).toEqual(true)
  })
})
