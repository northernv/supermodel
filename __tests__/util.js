const { default: Util } = require('../util')

it('should convert toSnake', () => {
  expect(Util.toSnake({ camelCase: true, PascalCase: true })).toEqual({ camel_case: true, pascal_case: true })
  expect(Util.toSnake({ camelCase: true, PascalCase: true }, 'prefix')).toEqual({ 'prefix.camel_case': true, 'prefix.pascal_case': true })
})
